##The Installation Matrix 

---------
	
An application that facilitates quoting implementation projects.  The Installation Matrix software makes quoting projects both easy and efficient.  The Installation Matrix allowed for consistent estimates for all projects. 

**Installation:**

You need node.js and npm. At the root of the project type:

```node
npm install
```

**Run Project:**

```node
npm run start
```

**Software Used:**

1. CSS3 (BootStrap http://getbootstrap.com)
1. HTML5
1. Angularjs (https://angularjs.org) use for MVVM
1. Yeoman (http://yeoman.io) workflow tooling for kickstarting new projects

----------
**Example:**

- Dashboard page allows the selection of a product simply by checking the box next to the product.
- The project hours are updated real-time
- Real-time searching exist on Dashboard page for quicker quotes
- Details on each product are just a click away
	

![Installation_Matrix](Installation_Matrix.gif)