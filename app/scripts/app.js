'use strict';

angular.module('installationMatrixApp', ['ngRoute','ngTouch','ngResource','ngAnimate','ui.bootstrap'])
  .config(function ($routeProvider,$tooltipProvider) {
        $tooltipProvider.options({
            appendToBody: true,
            placement: 'bottom'
        })
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/checkout', {
        templateUrl: 'views/checkout.html',
        controller: 'CheckoutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })

