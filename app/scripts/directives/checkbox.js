'use strict';

angular.module('installationMatrixApp')
    .directive('checkbox', function (checkout) {
        return {
            template: '<i ng-class="class" ng-click="toggle()"></i>',
            restrict: 'E',
            replace: true,
            scope: {
                add: '&',
                remove: '&',
                product: '=',
                checked: '@'
            },
            controller: function ($scope) {

            },
            link: function postLink(scope, element, attrs) {
                attrs.$set('objId', scope.$id)
                scope.class = "icon-check-empty"
                attrs.$observe('checked',function(value){
                     
                    scope.class = "icon-check-empty"
                    if(value === 'yes'){
                        scope.class = "icon-check"
                    }
                })

                scope.toggle = function () {
                    if (scope.class === "icon-check-empty") {
                        scope.class = "icon-check"
//                        attrs.$set('checked', 'yes')
                        scope.add();
                    } else {
                        scope.class = "icon-check-empty"
//                        attrs.$set('checked', 'no')
                        scope.remove();
                    }

                }


            }
        };
    });
