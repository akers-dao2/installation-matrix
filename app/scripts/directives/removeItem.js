'use strict';

angular.module('installationMatrixApp')
    .directive('closeButton', function () {
        return {
            template: '<button type="button" class="close pull-right" aria-hidden="true" ' +
                '>&times;</button>',
            restrict: 'E',
            replace: true,
            scope: {
                reducetotal: '&'
            },
            link: function (scope, element, attrs) {
                element.on('click', function (e) {
//            console.log(e.currentTarget)
                    angular.element(e.currentTarget).parent().css('display','none')
                    scope.reducetotal()
                });
            }
        };
    });
