'use strict';

angular.module('installationMatrixApp')
    .directive('modal', function () {
        return {
            templateUrl: '../../views/modal.html',
            replace: true,
            restrict: 'E',
            link: function postLink(scope, element, attrs) {

            }
        };
    });
