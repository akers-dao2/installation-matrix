'use strict';

angular.module('installationMatrixApp')
    .controller('MainCtrl', function ($scope, productChooser, $location, $cacheFactory, checkout,
                                      $rootScope, $anchorScroll,$filter,$window) {
        //list of items with hours
        $scope.items = [];
        //list of products to choose from

        $scope.$on('productChoose', function () {
            //grab products
            console.log(productChooser.get());
            $scope.products = productChooser.get()
            //reset store value
            productChooser.reset();
            //reset items count
            $scope.items = [];
        });

        $scope.totalHours = 0;
        //scope functions
        //add item to the $scope.items
        $scope.addItem = function (product) {
            product.checked = 'yes';
            checkout.setItems(product);
//            console.log(product)
            $scope.items = checkout.getItems();
        };
        //remove item from the $scope.items
        $scope.removeItem = function (product) {
            var match = 0;
            product.checked = 'no';
            angular.forEach($scope.items, function (v, i) {
                if (v.$$hashKey === product.$$hashKey) {
                    match = i;
                }
            });
            $scope.$broadcast('uncheck', product.Product);
            $scope.items.splice(match, 1);
        };
        // $watch the items variable and then update total as the items variable updates
        $scope.$watchCollection ('items', function (o, n) {
            //if the new value is not equal to old value
//            if (n !== o) {
                //total hours
                var totalHours = 0;
                //loops through the items array and sums the hours
                angular.forEach($scope.items, function (v, i) {
                    totalHours = v.TotalHours + totalHours
                });
                //set the total hours
                $scope.totalHours = totalHours;
//            }
        });

        // $watch the items variable and then update total as the items variable updates
        $scope.$watch('products', function (n, o) {
            //if the new value is not equal to old value
            if (n !== o) {

            }
        });

        //checkout button method.  Navigates to the checkout screen
        $scope.checkOut = function () {
            //pulls the business unit
            var businessUnit = $cacheFactory.get('businessUnit').get('value');
            if ($scope.totalHours > 0) {
                //set the checkout service with the total hours and business unit
                checkout.set({totalHours: $scope.totalHours, businessUnit: businessUnit, products: checkout.getItems()});
            }
            //reset the nav bar business drop down box to business unit
            $rootScope.$broadcast('reset');
            //reset checkout items
            checkout.resetItems()
            //navigates to the checkout screen
            $location.url('checkout')
        }

        //scroll product to view when clicked on under selected product
        $scope.scrollToView = function (p) {
            var old = $location.hash();
            var val = $filter('removespace')(p)
            $location.hash(val);
            $anchorScroll();
            $location.hash(old);
            $window.scrollBy(0,-47)
        }

    });
