'use strict';

angular.module('installationMatrixApp')
    .controller('CheckoutCtrl', function ($scope, $location, $cacheFactory, checkout, $modal) {
        var checkOutItems = checkout.get();
        $scope.items = checkout.get();

        $scope.addNewItem = function () {
            $location.url('/')
        }
        $scope.reduce = function (a, i, x) {
            i.totalHours = i.totalHours - a;
            i.products.splice(x,1);
        }

        $scope.closeQuote = function(){
            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: ModalInstanceCtrl,
//                resolve: {
//                    items: function () {
//                        return $scope.items;
//                    }
//                }
            });
        }
        function ModalInstanceCtrl($scope,$modalInstance){
            $scope.cancel = function(){
                $modalInstance.dismiss('cancel')
            }
        }
    });
