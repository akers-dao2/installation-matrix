'use strict';

angular.module('installationMatrixApp')
    .controller('HeaderCtrl', function ($scope, productChooser, $cacheFactory,$rootScope,checkout,$location,$window) {
        $scope.menuname = 'Business Unit';

        var cache = $cacheFactory('businessUnit', {number: 3});

        $scope.menu = function (v) {
            if (v !== $scope.menuname) {
                if($location.path() === '/checkout'){
                    $location.url('/')
                }
                $scope.menuname = v
                productChooser.set(v)
                //reset checkout items
                checkout.resetItems()
                cache.put('value', v)
                $window.scrollBy(0,0)

            }

        }


        $scope.$on('reset', function () {
            $scope.menuname = 'Business Unit';
        })
    });
