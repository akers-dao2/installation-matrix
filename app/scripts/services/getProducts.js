'use strict';

angular.module('installationMatrixApp')
  .factory('getProducts', function ($http) {
    // Service logic
    // ...

    // Public API here
    return {
           get:function(database){
               var unit = $http({method: 'GET', url: 'data/'+database+'.json'});
               return  unit;
           }
    };
  });
