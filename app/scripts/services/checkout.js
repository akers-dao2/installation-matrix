'use strict';

angular.module('installationMatrixApp')
    .factory('checkout', function ($cacheFactory) {
        // Service logic
        // ...

        var arr = []
        var store = []

        // Public API here
        return {
            setItems:function(v){
                store.push(v)
            },
            getItems:function(){
                return store
            },
            resetItems:function(){
                store = [];
            },
            set: function (v) {
                arr.push(v);
            },
            get: function () {
                return arr;
            }
        };
    });
