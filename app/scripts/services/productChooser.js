'use strict';

angular.module('installationMatrixApp')
    .factory('productChooser', function ($rootScope, getProducts) {
        var store = [];
        return{
            set: function (v) {
                getProducts.get(v).success(function (d) {
//                _.unique(_.pluck(d.data,'Product'))
                    var p = _.unique(d.data, 'Product');
                    //loop through unique data
                    _.each(p, function (v) {
                        //filter all data by each unique item
                        var f = _.filter(d.data, function (d) {
                            return d.Product === v.Product
                        });
                        if (f.length > 1) {
                            var sum = 0;
                            var steps = [];
                            _.each(f, function (d) {
                                sum = sum + d.ImplementationHours;
                                steps.push(d)
                            });
                            v.TotalHours = sum;
                            v.productDetails = steps;
                            steps = [];
                        }
                        else{
                            v.TotalHours = f[0].ImplementationHours;
                        }

                        store.push(v)

                    });
                    $rootScope.$broadcast('productChoose')
                }).error(function (e) {
                        //error message
                        console.log(e)
                        store = [];
                        $rootScope.$broadcast('productChoose')
                    })

            },
            get: function () {
                return store
            },
            reset: function () {
                store = [];
            }
        }
    });
