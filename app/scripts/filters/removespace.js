'use strict';

angular.module('installationMatrixApp')
  .filter('removespace', function () {
    return function (input) {
      return input.replace(/\s/g,'');
    };
  });
